from django import forms
from datapool.models import Quota, Datapool, Container

class CreateQuotaForm(forms.ModelForm):
    class Meta:
        model = Quota
        # project nicht, das wird extern festgelegt und kann vom user nicht geändert werden.
        fields = ['datapool', 'space']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # find out which datapools already have a quota for this project.
        usedDatapoolIds = Quota\
            .objects\
            .filter(project=self.instance.project)\
            .values_list('datapool__id', flat=True)

        # each project can only have one quota per datapool,
        # so limit the list of datapools to create a new quota for
        # to the datapools which dont have a quota for this project yet.
        unconnectedDatapools = Datapool.objects.all().exclude(id__in=usedDatapoolIds)
        # save that into the field
        self.fields['datapool'].queryset = unconnectedDatapools

    def clean_quota(self):
        if self.cleaned_data['space'] < 1:
            raise forms.ValidationError('Quota-space value must be 1 or greater.')
        return self.cleaned_data['space']


class CreateContainerForm(forms.ModelForm):
    class Meta:
        model = Container
        fields = ['name', 'space', 'quota']


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        try:# if quota is already set, use that and do not allow the user to change it
            self.instance.quota #  will cause an exception if quota is not set
            #self.fields['quota'].widget.attrs['readonly'] = True
            self.fields['quota'].widget = forms.HiddenInput()
        except: # if quota is not yet set via params, restrict choice to quotas of this project
            #how to get stuff to herer?
            pass#self.fields['quota'].queryset = Quota.objects.filter(project_id=self.data['projectID'])

