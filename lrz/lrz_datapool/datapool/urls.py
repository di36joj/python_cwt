from django.conf.urls import url
from datapool import views


urlpatterns = [
    url(r'^$',
        views.main_views.index_view,
        name='index'
        ),

    # D A T A P O O L
    # function-based view
    url(r'^datapool/list/$',
        views.datapool_views.datapool_list_view,
        name='datapool_list'
        ),
    url(r'^datapool/(?P<pk>\d+)/$',
        views.datapool_views.datapool_detail_view,
        name='datapool_detail'
    ),
    # url(r'^datapool/create/$',
    #     views.datapool_views.datapool_create_view,
    #     name='datapool_create'
    #     ),
    # url(r'^datapool/delete/(\d+)/$',
    #     views.datapool_views.datapool_delete_view,
    #     name='datapool_delete'
    #     ),
    #this is a duplicate of the class-based update-view blow
    #url(r'^datapool/updateF/(\d+)/$',
    #    views.datapool_views.datapool_update_view,
    #    name='datapool_update_f'
    #    ),

    # class-based view
    url(r'^datapool/create/$',
        views.datapool_views_cb.Datapool_create_view.as_view(),
        name='datapool_create'
        ),
    url(r'^datapool/delete/(?P<pk>\d+)/$',
        views.datapool_views_cb.Datapool_delete_view.as_view(),
        name='datapool_delete'
    ),
    url(r'^datapool/updateC/(?P<pk>\d+)/$',
        views.datapool_views_cb.Datapool_update_view.as_view(),
        name='datapool_update'
    ),

    # P R O J E C T
    url(r'^project/list/$',
        views.project_views.ProjectListView.as_view(),
        name="project_list"
    ),
    url(r'^project/create/$',
        views.project_views.ProjectCreateView.as_view(),
        name="project_create"
    ),
    url(r'^project/(?P<pk>\d+)/$',
        views.project_views.ProjectDetailView.as_view(),
        name='project_detail'
    ),
    url(r'^project/update/(?P<pk>\d+)/$',
        views.project_views.ProjectUpdateView.as_view(),
        name='project_update'
    ),
    url(r'^project/delete/(?P<pk>\d+)/$',
        views.project_views.ProjectDeleteView.as_view(),
        name='project_delete'
    ),

    # Q U O T A
    url(r'^quota/create/forProject/(?P<project_id>\d+)/$',
        views.quota_views.QuotaCreateView.as_view(),
        name='quota_create'
    ),
    url(r'^quota/update/(?P<pk>\d+)/$',
        views.quota_views.QuotaUpdateView.as_view(),
        name='quota_update'
    ),
    url(r'^quota/delete/(?P<pk>\d+)/$',
        views.quota_views.QuotaDeleteView.as_view(),
        name='quota_delete'
    ),

    # C O N T A I N E R
    url(r'^container/list/Qt/(?P<quotaID>\d+)/$',
        views.container_views.ContainerListView.as_view(),
        name="container_list"
    ),
    url(r'^container/list/Qt/(?P<quotaID>\d+)/asSnippet/$',
        views.container_views.ContainerListSnippetView.as_view(),
        name="container_list_snippet"
    ),
    url(r'^container/create/forProject/(?P<projectID>\d+)/$',
        views.container_views.ContainerCreateView.as_view(),
        name="container_create_forProject"
    ),
    url(r'^container/create/onQuota/(?P<quotaID>\d+)/$',
        views.container_views.ContainerCreateView.as_view(),
        name="container_create_onQuota"
    ),
    url(r'^container/update/(?P<pk>\d+)/$',
        views.container_views.ContainerUpdateView.as_view(),
        name="container_update"
    ),
    url(r'^container/delete/(?P<pk>\d+)/$',
        views.container_views.ContainerDeleteView.as_view(),
        name="container_delete"
    ),

]
