from django.contrib import admin
from datapool.models import Datapool, Project, Quota, Container
# Register your models here.

@admin.register(Datapool)
class DatapoolAdmin(admin.ModelAdmin):
#    type_hr = Datapool.POOL_TYPE_CHOICES[that value from the current instance] # TODO how to do interactive stuff?
    list_display = ['name', 'created', 'description', 'owner']
    search_fields = ['name', 'description', 'owner']
    list_filter = ['pool_type']

@admin.register(Quota)
class QuotaAdmin(admin.ModelAdmin):
    list_display = ['project', 'datapool', 'space', 'created',]
    search_fields = ['project', 'datapool']

@admin.register(Container)
class ContainerAdmin(admin.ModelAdmin):
    list_display = ['name', 'space', 'used']
    search_fields = ['name']

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'created']
    search_fields = ['name']
#    fields = [''] # welche felder in den details angezeigt werden sollen
#    readonly_fields = [''] # ein subset von fields - kann dann nicht geändert werden.
