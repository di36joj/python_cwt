from django.shortcuts import reverse
from django.views import generic
from datapool.models import Datapool

allFields=['name', 'description', 'space', 'pool_type', 'owner',]

class Datapool_create_view(generic.CreateView):
    model = Datapool
    template_name = 'datapool/datapool/datapool_create.html'
    context_object_name = 'datapool'
    fields = allFields

    def get_success_url(self):
        return reverse('datapool:datapool_list')


class Datapool_update_view(generic.UpdateView):
    model = Datapool
    template_name = 'datapool/datapool/datapool_update.html'
    context_object_name = 'datapool'
    fields = allFields

    def get_success_url(self):
        context = dict(pk=self.object.id)
        return reverse('datapool:datapool_update', kwargs=context)



class Datapool_delete_view(generic.DeleteView):
    model = Datapool
    template_name = 'datapool/datapool/datapool_delete.html'
    context_object_name = 'datapool'
    fields = allFields

    def get_success_url(self):
        return reverse('datapool:datapool_list')
