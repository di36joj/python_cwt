from django.shortcuts import render
from datapool import models as dp_models

# Create your views here.

def index_view(request):

    pools = dp_models.Datapool.objects.all()
    context = dict(
        pools=pools,
    )

    return render(request, 'datapool/index.html', context)
