from django.shortcuts import reverse
from django.views import generic
from datapool.models.project import Project
from datapool.models.quota import Quota

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


class ProjectAuthMixin(object):
    def get_queryset(self):
        allowed_group_names = self.request.user.groups.all().values_list('name', flat=True)
        return Project.objects.filter(name__in=allowed_group_names)


@method_decorator(login_required, name='dispatch')
class ProjectListView(generic.ListView):
    model = Project
    template_name = 'datapool/project/project_list.html'
    context_object_name = 'projects'
    fields = ['name', 'created']

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     return context


class ProjectCreateView(generic.CreateView):
    model = Project
    template_name = 'datapool/project/project_create.html'
    context_object_name = 'project'
    fields = ['name']

    def get_success_url(self):
        return reverse('datapool:project_list')

#we decorate dispatch, because that is the first function that is called.
@method_decorator(login_required, name='dispatch')
class ProjectDetailView(generic.DetailView):
    model = Project
    template_name = 'datapool/project/project_detail.html'
    context_object_name = 'project'
    fields = ['name', 'created']

    def get_context_data(self, **kwargs):
        # es gibt auch context-processor-middlewares, die immer gewisse sachen in den context schieben.
        # django macht das automatisch zB mit dem user-object
        context = super().get_context_data(**kwargs)
        context['assigned_pools'] = Quota.objects.filter(project=self.object)
        return context


class ProjectUpdateView(generic.UpdateView):
    model = Project
    template_name = 'datapool/project/project_update.html'
    context_object_name = 'project'
    fields = ['name']

    def get_success_url(self):
        kwargs=dict(
            pk = self.object.pk
        )
        return reverse('datapool:project_update', kwargs=kwargs)


class ProjectDeleteView(generic.DeleteView):
    model = Project
    template_name = 'datapool/project/project_delete.html'
    context_object_name = 'project'
    fields = ['name']


    def get_success_url(self):
        return reverse('datapool:project_list')
