from django.views import generic
from datapool.models.quota import Quota
from datapool.models.project import Project
from django.shortcuts import get_object_or_404, reverse, redirect
from datapool.forms import CreateQuotaForm

class QuotaCreateView(generic.CreateView):
    model = Quota
    template_name = 'datapool/quota/create.html'
    # 1. alternative: v-- PROJECT missing! see form_valid() --v
    # 2. alternative: form_valid hatte ein problem mit dem model-clean, daher jetzt get_form_kwargs()
    #fields = ['datapool', 'quota'] # 3. alternative: keine auto-form verwenden, sondern selber eine bauen
    # ... mit
    form_class = CreateQuotaForm
    #success_url = reverse(FNORD) # problem. see get_success_url() --v


    def dispatch(self, request, *args, **kwargs):
        project_id = self.kwargs['project_id']
        self.project = get_object_or_404(Project, pk=project_id)
        return super().dispatch(request, *args, **kwargs)


    def get_context_data(self, **kwargs):
        """If you need to stick some extra vars into
        the context for the html-template to access, do it like this.
        """
        context = super().get_context_data(**kwargs)
        context['project'] = self.project
        context['existingQuotas'] = Quota.objects.filter(project = self.project)
        return context


    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        # the quota-form.instance holds the quota-model.instance.
        # we prefill this here with an unsaved quota-model.instance
        # so during the create-POST-request will find some data later on.
        # the advantage of doing this here, is that the data is already
        # available during validation / model-cleaning
        # during create-GET, this will do nothing.
        kwargs['instance'] = Quota(project=self.project)
        return kwargs


    def form_valid(self, form):
        """is called when form has validated successfully, before saving.
        We use this to attach important data to the request that was not
        entered by the user/browser but is necessary to the database
        --
        then we added clean() to the quota-model in the next step,
        which is run during validation. however that needs quota.project to be filled.
        HOWEVER, we fill that var here, which is AFTER the validation.
        so we bite our tail.
        for quota.projects to be filled BEFORE validation (so it can be used in
        model-cleaning), we use get_form_kwargs() --^
        and this function now does nothing
        """
        if False: #alternative 1, a bit complex
            # form.save() würde *abspeichern* und ein model-obj zurückgeben
            # mit commit=False gibt es nur das ausgefüllte model-obj zurück, OHNE zu speichern
            quota = form.save(commit=False)
            # project wurde dem user nicht präsentiert beim ausfüllen,
            # muss daher nachträglich angeheftet werden.
            quota.project = self.project
            quota.save()

            kw=dict(pk=quota.pk)
            return redirect(reverse('datapool:project_detail', kwargs=kw))

        if False: # alternative 2: a bit easier.
            # hack the project-model-instance we created earlier
            # into the unsaved model-instance created by the form-validation
            form.instance.project = self.project
            return super().form_valid(form)

        # alternative 3: let get_form_kwargs() do the job
        return super().form_valid(form)



    def get_success_url(self):
        """Man könnte auch das class-attribute success_url füllen,
        aber es führt zu circular imports wenn man da reverse() verwendet."""
        return reverse('datapool:project_detail', kwargs=dict(pk=self.project.pk))

class QuotaUpdateView(generic.UpdateView):
    model = Quota
    template_name = 'datapool/quota/update.html'
    context_object_name = 'quota'
    fields = ['space']

    def get_success_url(self):
        return reverse('datapool:project_detail', kwargs=dict(pk=self.object.project.pk))


class QuotaDeleteView(generic.DeleteView):
    model = Quota
    template_name = 'datapool/quota/delete.html'
    context_object_name = 'quota'

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['project'] = self.object.project

    def get_success_url(self):
        """Man könnte auch das class-attribute success_url füllen,
        aber es führt zu circular imports wenn man da reverse() verwendet."""
        return reverse('datapool:project_detail', kwargs=dict(pk=self.object.project.pk))

