from django.views import generic
from django.shortcuts import get_object_or_404, reverse

from datapool.models import Datapool
from datapool.models.container import Container
from datapool.models.quota import Quota
from datapool.models.project import Project
from datapool.forms import CreateContainerForm

class ContainerListTemplate(generic.ListView):
    model = Container
    fields = ['name', 'quota', 'space', 'used', 'updated', 'created', ]
    template_name = 'datapool/container/list.html'
    context_object_name = 'containers'

    def dispatch(self, request, *args, **kwargs):
        self.quota = get_object_or_404(Quota, pk=self.kwargs['quotaID'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.quota.container_set.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['project']  = self.quota.project
        context['datapool'] = self.quota.datapool
        context['quota'] = self.quota
        context['used'] = self.quota.space - self.quota.spaceLeft()
        context['free'] = self.quota.spaceLeft()
        return context


class ContainerListView(ContainerListTemplate):
    pass

class ContainerListSnippetView(ContainerListTemplate):
    template_name = 'datapool/container/listSnippet.html'


class ContainerCreateView(generic.CreateView):
    model = Container
    template_name = 'datapool/container/create.html'
    context_object_name = 'container'
    form_class = CreateContainerForm

    def dispatch(self, request, *args, **kwargs):
        self.quota   = None
        self.project = None

        if 'quotaID' in self.kwargs:
            quotaID = self.kwargs['quotaID']
            self.quota = get_object_or_404(Quota, pk=quotaID)
        elif 'projectID' in self.kwargs:
            self.project = get_object_or_404(Project, pk=self.kwargs.get('projectID'))
        else:
            raise Exception("How did that happen? we should have either quota or project here.")

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['quota']   = self.quota
        context['project'] = self.project
        return context


    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.quota:
            kwargs['instance'] = Container(quota=self.quota)
        return kwargs

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        if self.project:
            form.fields['quota'].queryset = Quota.objects.filter(project_id=self.project)
        return form

    def get_success_url(self):
        if self.project:
            return reverse('datapool:project_detail', kwargs=dict(pk=self.project.id))
        if self.quota:
            return reverse('datapool:container_list', kwargs=dict(quotaID=self.quota.id,))


class ContainerUpdateView(generic.UpdateView):
    model = Container
    fields = ['name', 'quota', 'space', ]
    template_name = 'datapool/container/update.html'
    context_object_name = 'container'
    def get_success_url(self):
        return reverse('datapool:container_list', kwargs=dict(quotaID=self.object.quota.id,))


class ContainerDeleteView(generic.DeleteView):
    model = Container
    fields = ['name', 'quota', 'space', ]
    template_name = 'datapool/container/delete.html'
    context_object_name = 'container'
    def get_success_url(self):
        return reverse('datapool:container_list', kwargs=dict(quotaID=self.object.quota.id,))
