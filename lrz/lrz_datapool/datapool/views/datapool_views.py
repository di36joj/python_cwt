from django.shortcuts import render, redirect, reverse, get_object_or_404
from datapool.models import Datapool
from django.forms import modelform_factory
from django.views import generic

allFields=['name', 'description', 'space', 'pool_type', 'owner',]

def datapool_list_view(request):
    pools = Datapool.objects.all()
    context = dict(pools=pools)
    return render(request, 'datapool/datapool/datapool_list.html', context)


def datapool_create_view(request):
    formclass = modelform_factory(Datapool, fields=allFields)

    if request.method == 'POST':
        form = formclass(request.POST)
        if form.is_valid():
            datapool = form.save()
            return redirect(reverse('datapool:datapool_list'))
        else:
            pass # if invalid just fall through to the last return. but now it has error-annotations.
    else:
        form = formclass()

    context = dict(
        form=form,
    )

    return render(request, 'datapool/datapool/datapool_create.html', context)


def datapool_detail_view(request, pk):
    datapool = get_object_or_404(Datapool, pk=pk)
    context = dict(
        datapool = datapool
    )
    return render(request, 'datapool/datapool/datapool_detail.html', context)


def datapool_update_view(request, pk):
    pool = get_object_or_404(Datapool, pk=pk)
    formclass = modelform_factory(Datapool, fields=['name', 'description', 'pool_type', 'owner',])

    if request.method == 'POST':
        form = formclass(request.POST, instance=pool)
        if form.is_valid():
            datapool = form.save()
            return redirect(reverse('datapool:datapool_list'))
        else:
            pass # if invalid just fall through to the last return. but now it has error-annotations.
    else:
        form = formclass(instance=pool)

    context = dict(
        form=form,
        datapool=pool,
    )

    return render(request, 'datapool/datapool/datapool_update.html', context)


def datapool_delete_view(request, pk):
    """This thing instantly deletes instances. beware!"""
    pool = get_object_or_404(Datapool, pk=pk)
    pool.delete()

    return redirect(reverse('datapool:datapool_list'))



















