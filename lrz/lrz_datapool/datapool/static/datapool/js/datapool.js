function loadQuotaDetails(event){
    $td = $(event.target)
    var quotaUrl = $td.closest('tr').attr('data-quotaUrl')
    $.ajax({
      url: quotaUrl,
      cache: true
    })
        .done(function( html ) {
              $( "#quotaDetailsHere" ).html( html );
              renderSpaceBar()
        });
}

function renderQuota(index, quota) {
    //console.log(quota);

    $tr = $('<tr>').attr('data-quotaUrl', quota['id'])
    $tr.append($('<td>').text(quota['projectName']))
    $tr.append($('<td>').text(quota['space']))
    $tr.append($('<td>').text(quota['spaceLeft']))
    $tr.click(loadQuotaDetails)

    $("div#quotaOverview table").append($tr)
}


function renderQuotas(quotas){
    $("div#quotaOverview").html('<table class="table"> <tr> \
        <th>Project</th>\
        <th>Space</th>\
        <th>Left</th>\
    </tr> </table>');
    $.each(quotas, renderQuota);

}
