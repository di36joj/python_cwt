from django.db import models
from django.core.exceptions import ValidationError

class Container(models.Model):
    # the name of the container
    name = models.CharField(max_length=255)
    # the quota the container is connected to
    quota = models.ForeignKey('datapool.quota')
    space = models.PositiveIntegerField(
        help_text="how much space does the container take from the quota"
    )
    # when it was created
    created = models.DateTimeField(auto_now_add=True)
    # how much of the space is used
    used = models.PositiveIntegerField(default=0)
    # when the used-value was last updated
    updated = models.DateTimeField(null=True, blank=True)


    def __str__(self):
        return "Container '{}' on '{}' for '{}', space {} Datas, {} used (updated {})".format(
            self.name,
            self.quota.datapool.name,
            self.quota.project.name,
            self.space,
            self.used,
            self.updated,
        )

    def free(self):
        return self.space - self.used

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        """verify that there is enough space to make this change"""
        spaceLeft = self.quota.spaceLeft()

        if self.pk: # is update
            currentspace = Container.objects.get(pk=self.pk).space
            spaceNeeded = self.space - currentspace
        else: # is create
            spaceNeeded = self.space

        if spaceLeft < spaceNeeded:
            msg = 'Cannot set container-space to {} Datas because that needs {} Datas, ' \
                  'but only {} Datas are available in Quota for Datapool "{}".'
            raise ValidationError(msg.format(self.space, spaceNeeded, spaceLeft, self.quota.datapool.name))

        return super().save(force_insert, force_update, using, update_fields)
