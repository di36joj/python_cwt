from django.db import models
from django.core.exceptions import ValidationError

__all__ = ['Quota']

class Quota(models.Model):
    datapool = models.ForeignKey('datapool.Datapool')
    project  = models.ForeignKey('datapool.Project')
    space    = models.PositiveIntegerField()
    created  = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return ' - '.join([
            self.project.name,
            self.datapool.name,
            str(self.spaceLeft())+' of ' + str(self.space)+' Datas left',
        ])

    def clean(self):
        """clean prüft die eingaben.

        hier wollen wir, dass es nicht möglich ist 2 quotas
        für den selben datapool angelegt werden können.
        """
        existing_quotas = Quota.objects.filter(project=self.project, datapool=self.datapool).exclude(id=self.id)
        if existing_quotas.exists():
            raise ValidationError('A Quota for this datapool already exists, edit that to make modifications or delete it to be able to create a new one.')


    def spaceLeft(self):
        """how much of the quota is still left with the current containers cretaed?"""
        # from datapool.models.container import Container
        # containers_on_this_quota = Container.objects.filter(quota=self)
        # oder:
        # while this model does not really know that containers exist
        # let alone that those are linked to it, django gives us the container_set because
        # IT knows.
        containers_on_this_quota = self.container_set.all()
        spaceLeft = self.space - sum(containers_on_this_quota.values_list('space', flat=True))
        #if spaceLeft < 0: email admins for help
        return spaceLeft
