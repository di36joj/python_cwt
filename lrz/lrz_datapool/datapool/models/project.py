from django.db import models

class Project(models.Model):
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def countDatapoolsLinked(self):
        from datapool.models.quota import Quota
        return Quota.objects.filter(project=self).count()

