from django.db import models
from django.shortcuts import reverse
from datapool.models.quota import Quota

# wenn von dem modul ein *-import stattfindet,
# dann wird nur das aus dieser liste wirklich importiert.
__all__ = ['Datapool']

class Datapool(models.Model):

    POOL_TYPE_DSS = 0
    POOL_TYPE_DSA = 1
    POOL_TYPE_CHOICES = (
        (POOL_TYPE_DSS, 'DSS'),
        (POOL_TYPE_DSA, 'DSA'),
    )

    # alle felder sind immer mandatory by default
    name = models.CharField(max_length=255)
    # blank ist für model-admin: ob das feld im auto-generated form leer sein darf
    # null ist ob das feld in der DB leer sein darf
    description = models.TextField(max_length=255, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    owner = models.CharField(max_length=255)
    pool_type = models.IntegerField(choices=POOL_TYPE_CHOICES, null=True, blank=True)
    space  = models.PositiveIntegerField(default=0)

    def clean(self):

        return super().clean()

    def __str__(self):
        return ' - '.join([
            self.name,
            self.POOL_TYPE_CHOICES[self.pool_type] if self.pool_type else 'NoType',
            'Total Datas: {}'.format(str(self.space)) ,
            'Datas Left: {} '.format(str(self.spaceLeft())),
        ])

    def spaceLeft(self):
        quotasOnThisDatapool = Quota.objects.filter(datapool = self)
        used = 0
        for q in quotasOnThisDatapool:
            used += q.space

        return self.space - used


    def get_serialized_quotas(self):
        import json
        output = []

        for quota in self.quota_set.all():
            element = dict(
                id = reverse('datapool:container_list_snippet', kwargs=dict(quotaID=quota.pk)),
                projectName = quota.project.name,
                space = quota.space,
                spaceLeft = quota.spaceLeft(),
            )
            output.append(element)

        return json.dumps(output)
