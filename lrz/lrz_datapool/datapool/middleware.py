from datapool.models import Project

class IdmGroupMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response


    def __call__(self, request):
        if request.user.is_authenticated() and not request.user.is_staff:
            request.allowed_projects = "Fnord."
        else:
            request.allowed_projects = "Foo."

        response = self.get_response(request)

        return response

