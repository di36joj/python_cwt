from django.conf.urls import url, include
from rest_framework import routers
from datapool_api.views import (
    ContainerViewSet,
    QuotaViewSet,
    DatapoolViewSet,
    ProjectViewSet,
)

container = routers.DefaultRouter()
quota     = routers.DefaultRouter()
datapool  = routers.DefaultRouter()
project   = routers.DefaultRouter()
container.register('containers', ContainerViewSet)
quota.register(    'quotas',     QuotaViewSet)
datapool.register( 'datapools',  DatapoolViewSet)
project.register(  'projects',   ProjectViewSet)

urlpatterns = [
    url(r'^', include(container.urls)),
    url(r'^', include(quota.urls)),
    url(r'^', include(datapool.urls)),
    url(r'^', include(project.urls)),
]
