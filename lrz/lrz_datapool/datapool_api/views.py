from rest_framework import viewsets
from datapool_api.serializer import (
    ContainerSerializer,
    QuotaSerializer,
    DatapoolSerializer,
    ProjectSerializer
)
from datapool.models import (
    Container,
    Quota,
    Datapool,
    Project,
)

class ContainerViewSet(viewsets.ModelViewSet):
    queryset = Container.objects.all()
    serializer_class = ContainerSerializer


class QuotaViewSet(viewsets.ModelViewSet):
    queryset = Quota.objects.all()
    serializer_class = QuotaSerializer


class DatapoolViewSet(viewsets.ModelViewSet):
    queryset = Datapool.objects.all()
    serializer_class = DatapoolSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer



