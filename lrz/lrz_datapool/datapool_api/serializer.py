from rest_framework import serializers
from datapool.models import Container, Quota, Datapool, Project

class ContainerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Container
        fields = ['name','space','quota','used','updated','url',]
        read_only_fields = ['used', 'updated']

class QuotaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Quota
        fields = ['datapool','datapool','project','space','spaceLeft','url',]

class DatapoolSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Datapool
        fields = ['name','description','space','url',]

class ProjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Project
        fields = ['name','url',]

