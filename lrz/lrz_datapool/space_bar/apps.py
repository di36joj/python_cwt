from django.apps import AppConfig


class SpaceBarConfig(AppConfig):
    name = 'space_bar'
