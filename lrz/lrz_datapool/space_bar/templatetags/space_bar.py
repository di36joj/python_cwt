from django import template
from django.conf import settings

register = template.Library()

@register.filter
def space_bar(quota, dimensions=''):
    """Simple percent-bar with two fields: sum of used + free"""
    return chooch(quota, dimensions, 'tags/space_bar/space_bar.html')

@register.filter
def space_barN(quota, dimensions=''):
    """percent-bar where each container has its own box + free."""
    return chooch(quota, dimensions, 'tags/space_bar/space_barN.html')

@register.filter
def space_barM(quota, dimensions=''):
    """complex bar like N-type, where each container additionally has container-used/free."""
    return chooch(quota, dimensions, 'tags/space_bar/space_barM.html')

def chooch(quota, dimensions, templateFile):
    if not hasattr(settings, 'CHART_JS_SRC'):
        return "space_bar broken: please set settings.py 'CHART_JS_SRC' with a URL " \
               "where to get chart.js version 2.6.0, like: " \
               "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.js"

    height = "120px"
    width  = "75%"

    if dimensions:
        dimensions = dimensions.split(',')
        if dimensions: width  = dimensions.pop(0)
        if dimensions: height = dimensions.pop(0)

    context = dict(
        width    = width,
        height   = height,
        chartSrc = settings.CHART_JS_SRC,
        quota    = quota,
        used     = quota.space - quota.spaceLeft(),
    )

    tpl = template.loader.get_template(templateFile)
    return tpl.render(context=context)
