

Recommended IDE
===================

Use the Pycharm IDE: https://www.jetbrains.com/pycharm/.
It is awesome.

* Always add the Python executable in venv/bin/Python as Project interpreter to enable Python autocompletion for imports.
* Mark your project folder(s) (the ones containing your code) as "Sources Root": right click on the folder --> Mark as --> Sources Root


Django crispy forms
=========================

Awesome library that will prettify your forms.

https://github.com/django-crispy-forms/django-crispy-forms

.. code::

    {% load crispyformstags %}

    ...

    <form>
        {{ form|crispy }}
    </form>