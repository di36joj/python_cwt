


Installation of the training project
======================================

The current version of the training project is located at
https://gitlab.schnapptack.de/trainings/lrz.git

.. code::

    git clone https://gitlab.schnapptack.de/trainings/lrz.git


Now create a virtualenv. Virtualenv is a virtual environment encapsulating
all Python dependencies of your project. This is important as every project
may have very specific requirements and package versions.

.. code::

    cd lrz
    virtualenv venv

If you dont have Python3 as your default you may specify the concrete Python version:

.. code::

    cd lrz
    virtualenev -p Python3


Now we install the requirements:

.. code::

    cd lrz
    pip install -r requirements.txt


We have mad it. We no can migrate our database and starting the development server:

.. code::

    cd lrz/lrz_datapool
    python manage.py migrate
    python manage.py runserver


In case you need to create an admin superuser, use the following management command:

.. code::

    python manage.py createsuperuser

