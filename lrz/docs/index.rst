.. LRZ documentation master file, created by
   sphinx-quickstart on Sun Aug  6 14:59:51 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LRZ's documentation!
===============================

This documentation covers all relevant topics covered at the django training.
Django has an excellent documentation. So it's always worth to read it:
https://docs.djangoproject.com/en/1.11/


Getting started
======================

The project is developed using Python 3.5.
To develop this project it is recommended to use the Pycharm IDE:
https://www.jetbrains.com/pycharm/.
There is a free 30 days test version.

.. toctree::
   :maxdepth: 1

   content/installation.rst

Training Log
================================

.. toctree::
   :maxdepth: 1



   content/day_1.rst


List of recommended resources and third party apps
===================================================

.. toctree::
   :maxdepth: 2

   content/resources.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
