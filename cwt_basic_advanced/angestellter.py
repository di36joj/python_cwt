#!/usr/bin/python3

from person import Person

class Angestellter(Person):
    
    __slots__ = (
        '__gehalt'
    )
    
    def __init__(self, gehalt=-1, **aBunchOfKeywordArgsToPassOn):
        Person.__init__(self, **aBunchOfKeywordArgsToPassOn)
        self.__gehalt = gehalt
        
    def __getG(self): return self.__gehalt
    def __sG(self,gehalt:int): 
        self.__gehalt = gehalt
        return self
    gehalt = property(__getG, __sG)
    
    def __str__(self, *args, **kwargs):
        return "%s: Gehalt: %d, %s" % (
            self.__class__.__name__,
            self.gehalt, 
            super(Angestellter,self).__str__() #call parent's str()
        )
    