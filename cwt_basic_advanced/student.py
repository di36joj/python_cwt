#!/usr/bin/python3

from person import Person

class Student(Person):
    
    __slots__ = (
        '__bafoeg'
    )
    
    def __init__(self, bafoeg=-1, **aBunchOfKeywordArgsToPassOn):
        Person.__init__(self, **aBunchOfKeywordArgsToPassOn)
        self.__bafoeg = bafoeg
        
    def __getBafoeg(self): return self.__bafoeg
    def __sB(self,bafoeg:int): 
        self.__bafoeg = bafoeg
        return self
    bafoeg = property(__getBafoeg, __sB )
    
    def __str__(self, *args, **kwargs):
        return "Student: Bafög: %d, %s" % (self.bafoeg, super(Student,self).__str__())
     