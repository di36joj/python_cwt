#!/usr/bin/python3

class AdrDb(object):
    def __init__(self):
        self.db = []
        
    def add(self, toEnter):
        self.db.append(toEnter.getAdresse())
    
    def __len__(self):
        return len(self.db)
    
    def __str__(self, *args, **kwargs):
        bucket = []
        for adresse in self.db:
            bucket.append(str(adresse))
        return "AdrDb, entries: %d\n%s" % (
            len(self),
            "--\n".join(bucket)
        )
    
###########################################################################
if __name__ == '__main__':
    import student, angestellter, person, firma
    
    ab = AdrDb()
    
    ab.add(firma.Firma('netapp'))
    ab.add(student.Student(name='Riedel',vname='Hans',strasse='Koopmannstr 109', ort='Meiderich', tel1='424462'))
    ab.add(angestellter.Angestellter(name='Heisterkamp', ort='Meiderich'))
    ab.add(person.Person('kalk', 'chris'))
    
    print(str(ab))