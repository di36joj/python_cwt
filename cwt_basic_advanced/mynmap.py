#!/usr/bin/python3

import os, sys, time, re
import subprocess

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def d(m):
    #if __debug__: print("DEBUG:",m)
    pass
    

class MyNmap(object):
    
    def __init__(self, argv = None):
        
        
        #data structure of map:
        #map[$host = 'w.x.y.z"][$port = 22] = $timeWhenOpened
        #$timeWhenOpened = 0 = at $programStartTime
        self.__map = {}
        
        # store messages here
        self.__protcolFile = ''
        
        # if the scanner should run continously
        self.__daemonMode = False
        
        # which network to look at
        self.__toScanNetwork = ''
        
        # which hosts to scan
        self.__toScanHosts = set()
        
        #when did this program start.
        #used to calculate how long ports have been open when they disappear
        self.__programStartTime = time.time()
        
        # for on screen historical events
        # data-structure: buffer[n] = set($time, $status = new|add|del, message)
        self.__logBuffer = []
        self.__logBufferLast = []
        
        # every port logs when it was first seen open.
        # during the initial scan we don't know how long it was already open.
        # so we need to log this in a special way (the date-value will be zero).
        # so we need to know if this is the initial scan or a repeat scan
        self.__initialScan = True
        
    #########################
    # S E T T I N G S '
    #########################
        
        # how long to wait between deamon-mode refreshes
        self.__deamonModeDelayMs = 5000
        
        # which ports to scan.
        # the more you add, the slower bigger network-scans will be.
        # up until the point where they basically do not finish anymore because 
        #  the scanned host will block your requests.
        self.__portList = set([22,80])
        
        # REs to match the hostnames / IPs
        self.__reNamedHost = re.compile(r'Nmap scan report for (\S+) .(\S+).')
        # an empty parens to have 2 match-groups as with the named RE
        self.__reUnnamedHost = re.compile(r'Nmap scan report for ()(\S+)') 
        
        # maximum number of lines in the history buffer
        self.__maxLastBufferSize = 45

    #############
    # I N I T
    #############
        
        if argv: 
            self.parseArgv(argv)
        
        if self.__daemonMode:
            self.daemonize()
            
    def __del__(self):
        print(bcolors.ENDC)
    
    
    def parseArgv(self, argv:list):
        # -p <proto-file.txt>
        # -d == daemon-mode
        # -n <network/netmask>
        # <hostOrIp> ... <multipleIPOrHost>
        i = 1 # start at 1 because argv[0] is the scriptname
        while i < len(argv):
            param = argv[i]
            if param == "-p":
                i += 1
                self.__protcolFile = argv[i]
                d("protcol file is now " + self.__protcolFile)
            elif param == '-d':
                self.__daemonMode = True
                d("daemon-mode on")
            elif param == '-n':
                i += 1
                self.__toScanNetwork = argv[i]
                d('Network to scan' + self.__toScanNetwork)
            else: # if nothing matched must be host then
                self.__toScanHosts.add(param)
                d('Added host to scan:' + param)
                
            i += 1
            
    
    def __getTimestamp(self):
        # during the initial scan we don't really know how long the port has been open.
        # so we log a zero, so we know later that we don't know, eh?
        if self.__initialScan: return 0
        else: return time.time()
    time = property(__getTimestamp)
    
    def scan(self):
        if self.__toScanNetwork:
            # okies, scan networks!
            self.scanNetwork()
        else:
            for host in self.__toScanHosts:
                self.scanHost(host)
        
        # once the scan is through, every next scan will not be initial anymore.
        self.__initialScan = False
                
    def scanNetwork(self):
        raise(Exception("scan network not implemented yet"))
    
    def scanHost(self, host:str):
        """ scan a single host and update the map
        """
        
        #build command-line
        if self.__portList:
            ports = ','.join([str(port) for port in self.__portList])
        else:
            ports = ''
            
        args = "%s %s" % (ports, host)
        commandSpec = ['/usr/bin/nmap', '-p', ports, host]
        commandString = ' '.join(commandSpec)
        d("will execute scan as " + commandString)
        
        #execute scan and exfiltrate nmap's output to string-var
        result = subprocess.check_output(commandSpec).decode('utf-8')
        d(result)        
        
        #parse returns
        ''' like so:
            Starting Nmap 7.01 ( https://nmap.org ) at 2017-05-11 15:37 CEST
            Nmap scan report for badwlrz-kv00200.ws.lrz.de (10.156.82.7)
            Host is up (0.00078s latency).
            PORT   STATE  SERVICE
            22/tcp open   ssh
            80/tcp closed http
                                                <- empty line
            Nmap scan report for 10.156.82.65
            Host is up (0.00099s latency).
            PORT   STATE  SERVICE
            22/tcp open   ssh
            80/tcp closed http
            
        '''
        if "Host seems down." in result:
            self.__logHostDown(host)
            return
        
        self.__parseAndHandleNmapResult(host, result)
        
        
    def __parseAndHandleNmapResult(self, host:str, result:str) -> None:
        ''' cut nmap output into chapters,
            then have all chapters parsed and inserted into map
        '''
        hostOutputStarted = False
        bucket = []
        chapters = []
        for line in result.split("\n"):
            if line.startswith('Nmap scan report'):
                hostOutputStarted = True
            
            if not hostOutputStarted:
                continue
            
            if not line: # empty line signifies end of host-block
                chapters.append(bucket)
                bucket = []
                
            if "Nmap done:" in line: # that means the output is done
                self.p('nfo', line)
                break
            
            bucket.append(line)
            
        for chapter in chapters:
            self.__parseNmapHostOutput(chapter)
            
            
    def __parseNmapHostOutput(self, chapter):
        portsStarted = False
        host = '?'
        name = ''
        for line in chapter:
            if line.startswith('PORT '):
                portsStarted = True
                 # nice that we reached the ports, but we actually do not want to process the header line
                continue
            
            match = re.search(self.__reNamedHost,line)
            if not match:
                match = re.search(self.__reUnnamedHost,line)
                
            if match:
                name = match.group(1) #TODO use name for something clever
                host = match.group(2)
            
            if portsStarted:    
                #enter returns into map
                (port,status) = self.__splitNmapPortLine(line)
                if 'open' in status:
                    self.__logOpenPort(host, port)
                else:
                    self.__logClosedPort(host, port)
                
    def __splitNmapPortLine(self, line:str):
            pieces = line.split('/')
            if len(pieces) < 2:
                raise(Exception("Strange. '%s' should have split into two!" % line))
            
            return (int(pieces.pop(0)), pieces.pop(0))

                
    def __logOpenPort(self, host:str, port:int) -> None:
        if host not in self.__map:
            self.p('new', 'Host %s has been found' % host)
            self.__map[host] = {}
        else:
            pass # host is already known
            
        if port not in self.__map[host]:
            self.__map[host][port] = self.time
            self.p('add', 'Host %s has new open port %s' % (host, port))
        else:
            pass # port is already known
            
   
   
    def __logClosedPort(self, host:str, port:int) -> None:
        if host not in self.__map:
            # no need to log ports for hosts which we don't know.
            # FIXME not sure if this would qualify as a data corruption tho...
            d("WARNING: something strange happened.")
            pass
        else: # host is known, continue working
            if port not in self.__map[host]:
                pass
            else:
                self.p('del', 'Host %s has closed port %s, was open %s' % (
                    host,
                    port,
                    self.__getPortOpenDurationString(self.__map[host][port])
                ))
                del self.__map[host][port]
                
        self.__cleanEmptyHost(host)
        
    
    def __logHostDown(self, host:str) -> None:
        """ Will delete all ports from the host
        """
        if host not in self.__map:
            return
        
        for port in self.__map[host]:
            self.__logClosedPort(host, port)
        
        self.__cleanEmptyHost(host)
    
    def __cleanEmptyHost(self, host:str) -> None:
        if host in self.__map and len(self.__map[host]) == 0:
            del self.__map[host]
            self.p('del', "Host %s has no more open ports" % host)
            
    def __getPortOpenDurationString(self, openTime:float) -> str:
        if not openTime:
            return "since beginning of scan"
        else:
            return "for %.0f seconds" % (time.time() - openTime)
    
    def p(self, status, *messages):
        """ record an event to file and internal log-buffer
        """
        if status not in ('new', 'add', 'del', 'nfo'):
            raise(Exception("bad status value"))
        
        #log to file
        if self.__protcolFile:
            open(self.__protcolFile, 'a').writeline(time.time(), status, " ".join(messages)).close()
            
        #log to message-buffer
        self.__logBuffer.append((time.time(), status, " ".join(messages)))
        
    def printLog(self, log=None):
        if log is None:
            log = self.__logBuffer
            
        for (timestamp, status, message) in log:
            if status == 'new':
                color = bcolors.OKBLUE
            elif status == 'del':
                color = bcolors.FAIL
            elif status == 'add':
                color =  bcolors.OKGREEN
            else:
                color = ''
            
            print("%s: %s%s - %s%s" % (
                time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(timestamp)),
                color,
                status,
                message,
                bcolors.ENDC
            ))
    
    def printMap(self):
        for host in self.__map:
            print(bcolors.HEADER + "HOST: "+host + bcolors.ENDC)
            for port in self.__map[host]:
                print("\t%5d open since %s" % (port, self.__getPortOpenDurationString(self.__map[host][port])))
    
    def daemonize(self):
        while True:
            self.loop()
            time.sleep(self.__deamonModeDelayMs / 1000)
            
    def loop(self):
        self.scan()
        
        os.system('clear')
        self.printMap()
        print(bcolors.HEADER + "v v v v v                    L A S T   S C A N                     v v v v v" + bcolors.ENDC)
        self.printLog(self.__logBuffer)
        print(bcolors.HEADER + "v v v v v              H I S T O R I C A L   D A T A               v v v v v" + bcolors.ENDC)
        self.printLog(self.__logBufferLast)
        
        #roll logs after scan + print
        self.__logBufferLast = self.__logBuffer + self.__logBufferLast
        self.__logBuffer = []
        
        #cut last buffer down to size
        while len(self.__logBufferLast) > self.__maxLastBufferSize:
            # pop stuff off the end until the size is right
            self.__logBufferLast.pop()


if __name__ == "__main__":
    nm = MyNmap(sys.argv)
    nm.scan()
    nm.printLog()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    