#!/usr/bin/python3
import random

class ReadOnlyAttr(object):
    def __init__(self):
        # initialize internal key so that it is not the same for every instance
        self.__key = random.getrandbits(128)

        
        self.v = 'var'
        
        self.__setattr__('_const','readonly, biatch!',self.__key)
        
    def __setattr__(self, name, value, secret=None):
        print("Setting attr %s to %s" % (name, value))
        # if _const is written to 
        if name == '_const':
            # internal key must be set and given secret must be equal to internal key
            if (self.__key is None or secret != self.__key):
                raise(Exception("key-fault: Cannot set read-only attribute %s to value '%s'" % (name, value)))
            
        return object.__setattr__(self, name, value)
    
    def __getattribute__(self, name):
        if name == '__key':
            raise(Exception("Cannot hand out secret attribute "+name))
        
        return object.__getattribute__(self, name)

ro = ReadOnlyAttr()

ro.v = 'foo'
print(ro.v)
print(ro._const)
try:
    print(ro.__key)
except Exception as e:
    print("EGGSEPTSCHUN!",e)

try:
    ro._const = 'haxx0rd'
except Exception as e:
    print("EGGSEPTSCHUN!",e)
