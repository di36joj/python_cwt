#!/usr/bin/python3
# Chris version

import sys
class RwCsv(object):
    __slots__ = (
        '__headers',
        '__phonebook',
        '__iteratorPointer'
    )

    
    def __init__(self, filename = '', fileHasHeaders = True):
        # a list of the CSV-headers 
        self.__headers = []
        
        # a list of Person
        self.__phonebook = []
        
        if filename:
            self.einlesen(filename, fileHasHeaders)
    
    def __iter__(self):
        self.__iteratorPointer = 0
        return self
    def __next__(self):
        if self.__iteratorPointer > len(self.__phonebook) -1:
            raise StopIteration
         
        nextP = self.__phonebook[self.__iteratorPointer]
        self.__iteratorPointer += 1
        
        return nextP

    def __len__(self):
        return len(self.__phonebook)
    
    def __str__(self, *args, **kwargs):
        return ("RwCsv Telefonbuch, %d Einträge, %d Headers" % (len(self), len(self.__headers)))
        
    def setDb(self, headers, phonebook):
        if self.__headers or self.__phonebook:
            raise Exception('Cannot call setDb() on instance that already has data loaded.')
        
        self.__headers = headers
        self.__phonebook = phonebook
    
    def readHeaders(self, filename):
        fh = open(filename)
        self.__headers = fh.readline().strip().split(',')
        fh.close()
        
        return self
    
    
    def importPhonebook(self, filename, fileHasHeaders = True):
        import person
        fh = open(filename)
    
        if fileHasHeaders:
            fh.readline() # skip first line if fileHasHeaders
    
        # reset phonebook in case of re-running import
        self.__phonebook = []
        expectedElementCount = len(self.__headers)
        for line in fh:
            values = line.strip().split(',')
            if len(values) != expectedElementCount:
                print(
                    "WARNING: Line \n\t'%s'\n\t did not split into %d elements! Line will be igored"
                    % (line, expectedElementCount)
                )
                continue
            pDict = dict(zip(self.__headers, values))
            pObj  = person.Person().importDict(pDict)
            self.__phonebook.append(pObj)
    
        fh.close()
        return self
    
    def createCSV(self, sep=','):
        ob = [] # output buffer
        ob.append(str(sep.join(self.__headers)))
        for person in self.__phonebook:
            tempList = []
            pDict = person.getDict(self.__headers)
            
            # to make sure that the values from the dict are stored in the
            # right order, we grab them by iterating over the headers
            [ tempList.append(pDict[key]) for key in self.__headers]
                
            csvLine = str(sep.join(tempList))
            ob.append(csvLine)
            
        return '\n'.join(ob) + "\n"
    
    
    def einlesen(self, filenameIn, filenameHasHeaders=True):
        self.readHeaders(filenameIn) \
            .importPhonebook(filenameIn, filenameHasHeaders) 
        return self
    
    ''' old elite haxx0r way
    def e(self, f):
        c = open(f).read().strip().split('\n')
        h = c.pop(0).strip().split(',')
        return (h, list(map(lambda l:dict(zip(h,l.strip().split(','))),c)))
    '''
    
    
    def ausgeben(self, outf=None, sep=','):
        if outf is None:
            print(self.createCSV(sep))
        else:
            fh = open(outf, 'w')
            fh.write(self.createCSV(sep))
            fh.close()
    
    
    def suchen(self, key, value):
        if not key in self.__headers:
            print("unbekannter key")
            sys.exit()
    
        resultSet = [ p for p in self.__phonebook if p.has(key, value)]
        
        result = RwCsv()
        result.setDb(self.__headers,resultSet)
        return result
    
###############################################################
# run this code when module is run directly (i.e. not imported)
###############################################################
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("USAGE: arg1 input-file")
        sys.exit()
    filenameIn = sys.argv[1]
    
    phonebook = RwCsv(filenameIn)
    
    phonebook.ausgeben()
    phonebook.ausgeben(outf='out.txt')
    
    searchResult = phonebook.suchen('ort','muenchen')
    searchResult.ausgeben(sep=';')
    searchResult.ausgeben(outf='out2.txt')
    
    
############################################################################
# M E G A   H A C K E R   I N D I Z I E R T E S   D I C T I O N A R Y
############################################################################
    from collections import defaultdict
    d = {}
    headers = phonebook.createCSV().split('\n').pop(0).strip().split(',')
    #print(headers)
    
    for i in headers:
        d[i] = defaultdict(list)
        for p in phonebook:
            d[i][p.__getattribute__(i)].append(p)
    #!!
    #d = [ i:defaultdict(list) [ i:[p.__getattribute__(i)].append(p) for p in phonebook] for i in headers ] 
            
    print('keys von d:'             ,d.keys())
    print('keys von d von ort'      ,d['ort'].keys())
    print('d von ort von münchen'   ,d['ort']['muenchen'])
    
    print("\nalle personen aus münchen:")
    for p in d['ort']['muenchen']:
        print(str(p))


##########################################
# T E S T I N G 
##########################################
    print("\n-- TESTING GO:")
    
    if open(filenameIn).read() == open('out.txt').read():
        print("filecompare OK")
    else:
        print("filecompare NOT ok")
        
    if len(phonebook) == 21: 
        print("phonebook count OK")
    else: 
        print("phonebook count NOT ok")
    
    if len(searchResult) == 6: 
        print("searchResult count OK")
    else: 
        print("searchResult count NOT ok")
    
    if searchResult.createCSV().count('muenchen') == 6:
        print("searchResult content OK")
    else:
        print("searchResult content NOT ok")
        
