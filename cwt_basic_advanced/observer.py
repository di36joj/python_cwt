#!/usr/bin/python3

class car():
    doors = {
            'fl': 'front left',
            'fr': 'front right',
            'rl': 'rear left',
            'rr': 'rear right',
            'tr': 'trunk',
            'ft': 'frunk'
        }

class Observer(object):
    def handleEvent(self, source, eventName, eventValue):
        raise(Exception("abstract!"))
    
class Observable(object):
    def __init__(self):
        self.__observerList = set()
        
    def subscribe(self, o:Observer):
        if not isinstance(o, Observer):
            raise
        self.__observerList.add(o)
        
    def unsubscribe(self, o:Observer):
        if not isinstance(o, Observer):
            raise
        self.__observerList.remove(o)
    
    def createEvent(self, eventName, eventValue):
        for observer in self.__observerList:
            observer.handleEvent(self, eventName, eventValue)
    
class Combi(Observer, Observable):
    def __init__(self):
        Observable.__init__(self)
        
        # copy the correct set of doors and init them to false
        self.__doorOpen = car.doors.copy()
        for door in self.__doorOpen:
            self.__doorOpen[door] = False
        
        # these are the events this object can handle
        # everything else will be ignored
        self.__canProcessEvents = (
            'doorOpen',
            'doorClose'
        )
        
    def handleEvent(self, source, eventName, eventValue):
        if not isinstance(source, DoorSwitch):
            raise(Exception("Not the right type:" + str(type(source))))
        
        if eventName not in self.__canProcessEvents:
            return #cannot handle this event-type. jump out.
        
        if eventValue not in self.__doorOpen:
            raise(Exception("Door does not exist: "+eventValue))
        
        if eventName == 'doorOpen':
            print("Combi: door has opened: "+eventValue)
            self.__doorOpen[eventValue] = True
        elif eventName == 'doorClose':
            print("Combi: door has closed: "+eventValue)
            self.__doorOpen[eventValue] = False
            
        self.updateView(source)
        
    def updateView(self, source):
        """ Print an ascii graphic of the car's door status. """
        
        print( 'o-%s-o'  % ('[]' if self.__doorOpen['ft'] else '--'))
        print('%s    %s' % ('/'  if self.__doorOpen['fl'] else '|','\\' if self.__doorOpen['fr'] else '|'))
        print('%s    %s' % ('/'  if self.__doorOpen['rl'] else '|','\\' if self.__doorOpen['rr'] else '|'))
        print( 'l-%s-J'  % ('[]' if self.__doorOpen['tr'] else '--'))
        
        if source.toggleCount > 5:
            print("WARNING: Door %s is about to break! Stresslevel %s" % 
                  (car.doors[source.position], source.toggleCount)
            )
            self.createEvent('warningLight', 'on')
        
class Light(Observer):
    def __init__(self, color, text):
        self.color = color
        self.text = text
        self.on = False
        
    def handleEvent(self, source, eventName, eventValue):
        if eventName != 'warningLight':
            return
        
        if eventValue == 'on':
            self.on = True
            print("Light: %s is now glowing %s!" % (self.text, self.color))
        elif eventValue == 'off':
            self.on = False
            print("Light: %s is now off!" % (self.text))

    
class DoorSwitch(Observable):
    def __init__(self, position):
        Observable.__init__(self)
        
        if position not in car.doors:
            raise(Exception("door does not exist on this car: "+position))
        
        self.position = position
        self.__open = False
        self.toggleCount = 0
        
    def toggleDoor(self):
        """ Toggle the door's status and return the new value. """
        
        #toggle
        self.__open = not self.__open
        self.toggleCount += 1
        
        #trigger event for observers to handle.
        eventName = 'doorOpen' if self.__open else 'doorClose'
        
        self.createEvent(eventName, self.position)
        return self.__open
        
    
#########################################################

from random import randint

doors = {}
combi = Combi()
wLight = Light('orange', 'Warning Light')

combi.subscribe(wLight)

for position in car.doors:
    doors[position] = DoorSwitch(position)
    doors[position].subscribe(combi)

while True:
    # randomly open and close doors
    index = randint(0,5)
    position = list(car.doors.keys())[index]
    print("\nToggling door: %s" % position)
    doors[position].toggleDoor()    
    
    #ask human to press enter to continue so that steps develop in a controlled fashion
    input("continue?")
    
    
    
    
    
    
    
    
    