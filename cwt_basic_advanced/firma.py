#!/usr/bin/python3

from person import Person
from builtins import int
from random import random
import person
from adressierbar import Adressierbar
from rwcsv_chris import RwCsv

class Firma(Adressierbar):
    __slots__ = (
        '__name',
        '__mitarbeiter',
        '__maxPersonalNummer'
    )
    
    def __init__(self, name):
        self.name = name
        self.__mitarbeiter = {}
        self.__maxPersonalNummer = -1
        
    def __str__(self, *args, **kwargs):
        maListe = []
        for ma in self.__mitarbeiter.values():
            maListe.append(str(ma))
            
        return "Firma {} hat {} Mitarbeiter: \n{}" \
            .format(
                self.name, 
                len(self.__mitarbeiter), 
                "\n".join(maListe)+"\n"
            )
            
            
    def getAdresse(self):
        ''' Fulfill Adressierbar sub-class
        '''
        import adresse
        return adresse.Adresse(name=self.name) 
    
    
    def __getname(self): return self.__name
    def __setname(self, name): self.__name = name
    name = property(__getname, __setname)
    
    def __getNaechstePersonalnummer(self):
        self.__maxPersonalNummer += 1
        return self.__maxPersonalNummer
    
    def einstellen(self,p):
        if isinstance(p, Person):
            ma = p
        elif isinstance(p, str):
            ma = Person(p)
        else:
            raise(Exception('einstellen nimmt 1 parameter vom typ Person. wenns schnell gehen muss auch str'))
        
        pn = self.__getNaechstePersonalnummer()
        self.__mitarbeiter[pn] = ma

    def kuendigen(self, pn:int):
        if not isinstance(pn, int):
            raise(Exception('pn muss int sein'))
        if not pn in self.__mitarbeiter.keys():
            raise(Exception('Gibt keinen MA mit der ID. schon weg?? HAHAHAH!!'))
        del self.__mitarbeiter[pn]
        
    def massenentlassung(self, pc:int):
        from math import ceil
        
        if not isinstance(pc, int):
            raise(Exception('massenentlassung fordert eine prozentzahl in int'))
        
        maCount = len(self.__mitarbeiter)
        fireCount = ceil(maCount * (pc /100.0))
        print("FOR GREAT SHAREHOLDER-VALUE: DIE HALBJÄHRLICHE, ZUFÄLLIGE MASSENENTLASSUNG!")
        print("Aktuelle Mitarbeiterzahl ist {}, es werden {}% = {} gekündigt"
              .format(maCount, pc, fireCount)
        )
        for i in range(fireCount):
            self.__randomentlassung()
            
    def __randomentlassung(self):
        from random import randint
        
        maCount = len(self.__mitarbeiter)
        if not maCount:
            print("Oh, keiner mehr übrig!")
            return
        firePos = randint(0,maCount-1)
        firePn  = list(self.__mitarbeiter.keys())[firePos]
        fireMa  = self.__mitarbeiter[firePn]
        print("HUI! schüss %s! wir hoffen %s hat ein gutes Arbeitsamt!" % 
              (fireMa.name, fireMa.ort) 
        )
        del self.__mitarbeiter[firePn]
        
    def masseneinstellung(self, phonebook):
        for p in phonebook:
            self.einstellen(p)
        print("Masseneinstellung: %d personen eingestellt" % len(phonebook))
        
###############################################################################        

if __name__ == "__main__":
    import person, angestellter, student
    import rwcsv
    
    p1 = person.Person('kiendl', 'andy', ort='ebersberg')
    p2 = person.Person('kalk', 'chris', ort='heimstetten')
    s  = student.Student(vname='Hans', name='Riedel', bafoeg=1337)
    a  = angestellter.Angestellter(vname='Mia', name='Heisterkamp', gehalt=42)
    
    f = Firma('netapp')
    
    f.einstellen(p1)
    f.einstellen('ub1')
    f.einstellen('ub2')
    f.einstellen('ub3')
    f.einstellen(p2)
    f.einstellen(s)
    f.einstellen(a)
    print(f)
    f.kuendigen(3)
    print(f)
    
    phonebook = RwCsv('adr.txt')
    f.masseneinstellung(phonebook)
    print(f)
    f.massenentlassung(45)
    print(f)
        
        
        
        
        