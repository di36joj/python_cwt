#!/usr/bin/python3

class Adresse(object):
    def __init__(self, **bunchOfKeywords):
        self.allowedKWs = (
            'name',
            'vname',
            'strasse',
            'ort',
            'tel'
        )
        self.attrCount = 0
        
        for param in bunchOfKeywords:
            usage = " - allowed keyWordArgs: "+ ', '.join(self.allowedKWs)
            
            if 'name' not in bunchOfKeywords:
                raise(Exception("Mandatory argument 'name' missing."+usage))
             
            if param not in self.allowedKWs:
                raise(Exception("Unwanted keyword " + param + usage))
            
            value = bunchOfKeywords[param]
            if value is None: # do not store empty values
                continue
            
            self.__setattr__(param, value)
            self.attrCount += 1
        
    def __len__(self):
        return self.attrCount
    
    def __str__(self, *args, **kwargs):
        bucket = []
        
        if len(self) == 1: # if there is only one attribute
            # it must be name. just return that.
            return "%s (total attribute count: 1)\n" % self.name 
        
        for key in self.allowedKWs:
            if not hasattr(self, key):
                continue #don't work on keys which are not set
            
            value = self.__getattribute__(key)
            if key == 'name': # special treatment for name - gets printed as header
                bucket.append("%s (total attribute count: %d):" % (self.name, len(self)))
            else: #save into bucket
                bucket.append("%10s: %s" % (key, value))
                
        return "\n".join(bucket) + "\n"
        