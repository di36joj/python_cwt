#!/usr/bin/python3

#for access to the CLI arguments
import sys
import re

def printUsage():
    print("No params prints a 8x8 grid and asks the user for 1 piece from Stdin.")
    print("-h --help prints this help.")
    print("-s --size n sets the gridsize to n. between 1 and 26 incl.")
    print("-i --init puts the board into standard starting config. Will ignore all other pieces given.")
    print("Txy ... add as many pieces as you want in format [TSLDKBtsldkb][a-h][1-8].")


def argvParse(argv, size):
    pieces = []
    initStandardPieces = False

    n = 1
    while n < len(argv):
        if argv[n] == '-h' or argv[n] == '--help':
            printUsage()
        elif argv[n] == '-s' or argv[n] == '--size':
            n += 1 # jump to next arg, which is the size-parameter
            size = argvParseSize(argv, n)
        elif argv[n] == '-i' or argv[n] == '--init':
            initStandardPieces = True
        else: # no previous match? must be Txy piece
            pieces.append(moveParseTxy(argv[n]))
        n += 1
    return size, pieces, initStandardPieces


def argvParseSize(argv: list, n: int) -> int:
    """
    inspects the n-th argv and returns the valid, requested grid-size.
    if no valid grid-size could be determined, the function will
    print an informative message and exit.

    :param argv: list containing sys.argv
    :param n: int which element ov argv to look at
    :return: int the grid-size
    """
    # check if there is a parameter to the argument
    if len(argv) < n + 1: # +1 because len is count and n is zero-based
        print("Size parameter missing")
        sys.exit()

    # check if parameter is numeric
    if not argv[n].isnumeric():
        print("Size parameter must be numeric")
        sys.exit()

    # check if value is in bounds. max 26 because we run out of letter then
    size = int(argv[n])
    if size < 1 or size > 26:
        print("ERROR: Size is wrong. must be between 1 and 26 incl.")
        sys.exit()

    # nice! A-OK
    return size


def initBoard(size):
    # init dict called chess with the size of the board
    xRange = []
    for x in range(1,size+1):
        xRange.append(chr(x+96))
    yRange = []
    for y in list(range(1,size+1)):
        yRange.append(str(y))

    chess = {}
    # write down some things that will come in handy later on
    chess['size'] = size
    chess['xr']   = xRange
    chess['yr']   = yRange

    # iterate over x-range and y-range 
    for x in chess['xr']:
        for y in chess['yr']:
            # init all fields with dot
            chess[x+y] = '.'
    return chess

def initStandardPieces(chess):
    # this is the list of pieces in starting positions
    pieces = ['Ta1', 'Sb1', 'Lc1', 'Kd1', 'De1', 'Lf1', 'Sg1', 'Th1', # row 1 - black
              'Ba2', 'Bb2', 'Bc2', 'Bd2', 'Be2', 'Bf2', 'Bg2', 'Bh2', # row 2
              'ba7', 'bb7', 'bc7', 'bd7', 'be7', 'bf7', 'bg7', 'bh7', # row 7 - white
              'ta8', 'sb8', 'lc8', 'kd8', 'de8', 'lf8', 'sg8', 'th8'  # row 8
    ]
    for piece in pieces:
        t  = piece[0]
        xy = piece[1:]
        chess[xy] = t
    return chess


def printTableHeader(size):
    # print table header
    counterLtR = 0
    while counterLtR <= size:
        if counterLtR == 0:
            print (' ',end=" ")
        else:
            print (chr(96+counterLtR), end=" ")
        counterLtR += 1
    print() # bang out newline to conclude header


def printBoard(chess):
    printTableHeader(chess['size'])

    for y in chess['yr']:
        print (y, end=' ')
        for x in chess['xr']:
            print(chess[x+y], end=' ')
        print()
    print()


def moveAskPlayer(nextPlayer):
    turns = {'weiss':'SCHWARZ', 'SCHWARZ': 'weiss'}
    while True:
        move = input(nextPlayer+" zieht: ")
        if move == 'q':
            print("Quit")
            sys.exit()

        piece = moveParseTxy(move)

        if moveCheck(piece, nextPlayer):
            # move was valid, switch players, return move
            nextPlayer = turns[nextPlayer]
            return piece, nextPlayer
        else:
            pass # nothing happens. just ask user again.


def moveParseTxy(input):
    matches = re.match(r'^([TSLDKB])([a-h][1-8])$', input, re.I)
    piece = {}
    if matches:
        piece['t']  = matches.group(1)
        piece['xy'] = matches.group(2).lower()
    return piece

def moveCheck(piece, nextPlayer):
    if not piece:
        print("Ungültiger Zug, bitte neu eingeben [TSLDKB][a-h][1-8] oder q für quit")
        return False
    else:
        if nextPlayer == 'weiss' and piece['t'] in 'TSLDKB':
            print("Bitte Kleinbuchstaben verwenden um weisse Figuren zu ziehen.")
            return False
        if nextPlayer == 'SCHWARZ' and piece['t'] not in 'TSLDKB':
            print("Bitte Grossbuchstaben verwenden um SCHWARZE FIGUREN zu ziehen.")
            return False

    return True


def moveApply(chess, move):
    # unpack move
    t   = move['t']
    xyN = move['xy']

    # disambiguate and remove piece from old place
    boardClearOldPlace(chess,t)

    # check the condition of the new place
    boardCheckNewPlace(chess, xyN)

    # set piece into new location
    boardSetPiece(chess, xyN, t)
    return chess


def boardSetPiece(chess, xy, t):
    chess[xy] = t


def initApplyPieces(chess, pieces):
    for piece in pieces:
        if not piece:
            continue
        boardSetPiece(chess, piece[0], piece[1])
    return chess

def boardClearOldPlace(chess,t):
    # find old grid locations for piece
    oldPos = []
    for x in chess['xr']:
        for y in chess['yr']:
            if chess[x+y] == t:
                oldPos.append(x+y)

    # check if there is only one or multiple old locations
    xy0 = ''
    if len(oldPos) == 0:
        print("OI! Nicht schummeln! Vorhin gabs keinen",t,"mehr!")
    elif len(oldPos) == 1:
        xy0 = oldPos[0]
    else:
        xy0 = moveMultiselect(chess,oldPos)

    chess[xy0] = '.'


def boardCheckNewPlace(chess, xyN):
    t0 = chess[xyN]
    if t0 == 'k':
        print("SCHWARZ gewinnt!")
        sys.exit()
    elif t0 == 'K':
        print("weiss gewinnt!")
        sys.exit()
    elif t0 != '.':
        print("You captured",t0)
    else:
        pass


def moveMultiselect(chess,oldPos):
    matrix = chess.copy()
    for (i, xy) in enumerate(oldPos,1):
        matrix[xy] = i
    printBoard(matrix)
    while True:
        selection = input("Es gibt mehrere von diesem Typ. Bitte wählen Sie mit der Zahl 1 bis "+str(len(oldPos))+': ')
        if not selection.isnumeric():
            print("Die Eingabe war nicht numerisch.")
            continue
        selection = int(selection) - 1 # sub1 because arrays are zero-based
        if 0 <= selection < len(oldPos):
            return oldPos[selection]
        else:
            print("Die Eingabe ist ausserhalb des erlaubten Bereichs.")


###############################################################################

# settings
"""how big the square will be.
default is 8 x 8
"""
size  = 8

# if debug information will be printed into the output
debug = True

#handle parameters.
size, pieces, initStandardPiecesFlag = argvParse(sys.argv, size)

chess = initBoard(size)

# init board with standard config or pieces from CLI
if initStandardPiecesFlag:
    chess = initStandardPieces(chess)
else:
    chess = initApplyPieces(chess,pieces)

printBoard(chess)

nextPlayer="weiss"
log = []
while True:
    move, nextPlayer = moveAskPlayer(nextPlayer)
    log.append(move)
    chess = moveApply(chess, move)
    printBoard(chess)
