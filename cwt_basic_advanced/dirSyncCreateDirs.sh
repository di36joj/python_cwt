#!/bin/bash
mkdir --parents ./left/l1a/l11/l111
mkdir --parents ./left/l2a/l11/l112
mkdir --parents ./left/l3a/l11/l113
mkdir --parents ./left/l4a/l11/l114

touch ./left/l1a/l11/l111/L1
touch ./left/l1a/l11/l111/L2
touch ./left/l1a/l11/l111/L3
touch ./left/l1a/l11/l111/L4
touch ./left/commonNewerR
touch ./left/commonSameSame 


mkdir --parents ./right/r1a/r11/r111
mkdir --parents ./right/r2a/r11/r112
mkdir --parents ./right/r3a/r11/r113
mkdir --parents ./right/r4a/r11/r114

touch ./right/r1a/r11/r111/R1
touch ./right/r1a/r11/r111/R2
touch ./right/r1a/r11/r111/R3
touch ./right/r1a/r11/r111/R4
touch ./right/commonNewerL
touch ./right/commonNewerR
touch ./right/commonSameSame -r ./left/commonSameSame


touch ./left/commonNewerL
