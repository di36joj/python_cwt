#!/usr/bin/python3
import re

from adressierbar import Adressierbar

class Person(Adressierbar):
    # original fileheader
    # name,vname,strasse,ort,tel1,tel2,email,geb
    __slots__ = (
        '__name',
        '__vname',
        '__strasse',
        '__ort',
        '__tel1',
        '__tel2',
        '__email',
        '__geb',
        '__partner',
        '__objectNumber'
    )
    
    #class-attribute: 
    #can be read and modified everywhere like a normal attr.
    #however: all instances of this object share the same memory-addr == value
    instanceCount = 0 
    
    def __init__(self, name=None, vname=None, strasse=None, ort=None, tel1=None):
        self.__objectNumber = Person.instanceCount
        Person.instanceCount += 1
        
        self.__name    = name
        self.__vname   = vname
        self.__strasse = strasse
        self.__ort     = ort
        self.__tel1    = tel1
        self.__tel2    = None
        self.__email   = None
        self.__geb     = None
        self.__partner = None
        
        
    def __str__(self, *args, **kwargs):
        return self.stringRepresentation()

    def couplesStr(self):
        return self.stringRepresentation(listPartner=False)
    
    def stringRepresentation(self, listPartner=True):
        me = self.__class__.__name__ 
        if me == 'Person':
            objStatus = 'Selbstständiges Object Person' 
        else:
            objStatus = 'Vererbendes Object Person zu '+me
        
        if not listPartner:
            partnerStatus = ''
        else: 
            if self.__partner:
                partnerStatus = ", verheiratet mit "+self.__partner.couplesStr() 
            else: 
                partnerStatus = ', single'
            
        return "%s, %s %s aus %s, %s%s" %(
            objStatus,
            self.vname or '?',
            self.name or '?',
            self.strasse or '?',
            self.ort or '?',
            partnerStatus 
        )

    def __add__(self, partner):
        return self.heiraten(partner)
    
    def __del__(self):
        Person.instanceCount -= 1
            
    def headCount(self):
        '''tells you how many Person-objects are in memory and
        the how many-th object this is.
        '''
        return Person.instanceCount, self.__objectNumber
    
    def getAdresse(self):
        ''' Fulfill Adressierbar sub-class
        '''
        import adresse
        return adresse.Adresse(
            name=self.name, 
            vname=self.vname,
            tel=self.tel1,
            strasse=self.strasse,
            ort=self.ort
        )
    

    def importDict(self, rwcsvDict):
        for key in rwcsvDict.keys():
            attr = '_Person__' + key
            self.__setattr__(attr, rwcsvDict[key])
        return self
    def getDict(self, headers):
        pDict = {}
        for key in headers:
            attr = "_Person__"+key
            pDict[key] = self.__getattribute__(attr)
        return pDict
    def has(self, key, value):
        attr = '_Person__'+key
        return self.__getattribute__(key) == value
            
    def __getName(self): return (self.__name)
    def __setName(self, name=None): 
        if name is not None:  self.__name  = name
        return self
    name=property(__getName,__setName)

    
    def __getVName(self): return (self.__vname)
    def __setVName(self,vname=None): 
        if vname is not None: self.__vname = vname
        return self
    vname=property(__getVName,__setVName)
    
    
    def __getOrt(self): return self.__ort
    def __setOrt(self,s):
        self.__ort = s
        return self
    ort=property(__getOrt,__setOrt)

    
    def __getStrasse(self): return self.__strasse
    def __setStrasse(self,s):
        self.__strasse = s
        return self
    strasse=property(__getStrasse,__setStrasse)

    
    def __getEmail(self): return self.__email
    def __setEmail(self,s):
        matches = re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", s, re.IGNORECASE)
        if matches:
            self.__email = s
        else:
            raise(Exception("ERROR: email invalid"))
        return self
    email=property(__getEmail,__setEmail)
    
    def __getTel1(self): return self.__tel1
    def __setTel1(self,s):
        self.__tel1 = s
        return self
    tel1=property(__getTel1,__setTel1)
    
    def __getTel2(self): return self.__tel2
    def __setTel2(self,s):
        self.__tel2 = s
        return self
    tel2=property(__getTel2,__setTel2)
    
    def __getGeb(self): return self.__geb
    def __setGeb(self,n):
        import re
        matches = re.match(r"\d\d\.\d\d\,\d\d\d\d", n)
        if not matches:
            raise(Exception("Geburtsdatum erscheint komisch. Erlaubt: DD.MM.YYYY"))
        if   1    <= int(matches.group(1)) <= 31    \
         and 1    <= int(matches.group(2)) <= 12    \
         and 1900 <= int(matches.group(3)) <= 2020  :
            self.__geb = n
            return self
        raise(Exception('Geburtsdatum Format ist okay, aber Zahlen sind falsch. DD.MM.YYYY'))
    geb=property(__getGeb,__setGeb)
    
    def heiraten(self, partner):#
        if not isinstance(partner, Person):
            raise(Exception('Kann sowas nicht heiraten :('))
        if self == partner:
            raise(Exception("Liebe dich selbst. Ja, schon... Aber nicht heiraten, bitte."))
        if self.__partner == partner:
            raise(Exception("Schön, dass ihr euch immer noch liebt. Ihr seid schon verheiratet"))
        if self.__partner:
            raise(Exception("Bin schon mit jemand anderem verheiratet !"))
        self.__partner = partner
        partner.__partner = self
        
        return "It is done! %s hat %s geheiratet" % (self.name, partner.name)
        
    
if __name__ == "__main__":
    from inspect import currentframe, getframeinfo
    def ee(m='none'):
        import traceback
        print("ErrorExit: Message: ",m)
        [print(line.strip()) for line in traceback.format_stack()]
        exit()
        
    def testAlter(p):
        p.alter = 5
        if p.alter != 5: ee(getframeinfo(currentframe()).lineno)
        
        try:
            p.alter = 'zwölf'
            ee('exception missed') # no exc? problem!
        except Exception as e:
            pass # we wanted it to raise an exception
            
        try:
            p.alter = '-1'
            ee('exception missed') # no exc? problem!
        except Exception as e:
            pass # we wanted it to raise an exception
            
        try:
            p.alter = -1
            ee('exception missed') # no exc? problem!
        except Exception as e:
            pass # we wanted it to raise an exception
            
        p.alter = '0'
        if p.alter != 0: ee()
        
        p.alter = '199'
        if p.alter != 199: ee()
        
        p.alter = '200'
        if p.alter != 200: ee()
        
        try:
            p.alter = '201'
            ee('exception missed') # no exc? problem!
        except Exception as e:
            pass # we wanted it to raise an exception
            
        p.alter = 100
        if p.alter != 100: ee()
        
    
    def testEmail(p):
        p.email="a@b.com"
        if p.email != "a@b.com": ee()
                 
    print(" R E G R E S S I O N T E S T I N G :  G O !")
    
    p=Person()
    testAlter(p)
    testEmail(p)
    
    print(" R E G R E S S I O N T E S T I N G :  S U C E S S U L L Y   C O M P E T E D  !")
    
    
