#!/usr/bin/python3

import sys

def einlesen(fname):
    fh=open(fname,'r')
    header=fh.readline().rstrip()
    fields = header.split(sep=',')
    db = []
     
    for dline in fh:
        dfields = dline.rstrip().split(sep=',')
        row = {}
        for ind, dat in enumerate(dfields):
            row[fields[ind]] = dat
        db.append(row)
#        db.append(dict(zip(fields,
    return (fields,db)

def ausgeben(data,outf=sys.stdout,sep=','):
    if outf != sys.stdout:
        outf = open(outf,'w')

    (fields,db) = data
    header = sep.join(fields)
    print(header,file=outf)
    for row in db:
        rowl=[]
        for dfield in fields:
            rowl.append(row[dfield])
        print(sep.join(rowl),file=outf)
    if outf != sys.stdout:
        outf.close()


def suchen(k,v,data):
    (fields,db) = data
    erg = []
    for row in db:
        if row[k] == v:
            erg.append(row)
    return (fields,erg)

#filename=sys.argv[1]
#print("Einlesen...")
#res=einlesen(filename)
#(fieldnames,db)=res
#print("Ausgeben stdout")
#ausgeben(res,sep=';')
#print("Ausgeben file")
#ausgeben(res,"out.txt")
#print("Suchen ...")
#res1=suchen("ort","muenchen",res)
#print("Suchergebnis stdout")
#ausgeben(res1)
#print("Suchergebnis file")
#ausgeben(res1,"out2.txt")


