#!/usr/bin/python3

import os,sys,time
from macpath import dirname
from plainbox.impl.unit import file
from collections import namedtuple
from _ast import Compare

def usageAndExit():
    print("you will get 3 lists: left, both, right")
    print("need 2 params, both dirs to compare.")
    exit()


def d(m):
    if __debug__: print(m)
    

def collectFileSet(dirName):
    cwd = os.getcwd()
    os.chdir(dirName)
    fileSet = set()
    for (here, dirs, files) in os.walk('.'):
        if files: # dir has files
            # turn files into full path
            files = map(lambda x:here+'/'+x, files)
            # add files-list to fileSet via instant-set-union
            fileSet = fileSet.union(files)
        elif not files and not dirs: # dir is empty
            # just write down the empty dir
            fileSet = fileSet.union([here+"/"])
            
        
    os.chdir(cwd)
    return fileSet


def compareMtime(fileSet:set, ld:str, rd:str) -> set:
    """

    :param fileSet:
    :param ld:
    :param rd:
    :return:
    """
    instructionSet = set()
    instruction = namedtuple('CopyFile', "src dst")
    for file in fileSet:
        fnL = os.path.join(ld,file) 
        fnR = os.path.join(rd,file)
        
        tL = os.stat(fnL).st_mtime
        tR = os.stat(fnR).st_mtime
        
        diff = tL - tR
        
        if not diff: #both files the same mtime
            d ("SAME:            %s" % file)
            #do nothing
        elif diff < 0:
            d ("R->L: %10.5f %s" % (diff, file))
            instructionSet.add(instruction(fnR, fnL))
        else:
            d ("L->R: %10.5f %s" % (diff, file))
            instructionSet.add(instruction(fnL, fnR))
    return instructionSet
            
         
        


def printSet(setThing):
    for file in setThing: 
        print(file)

################################################################################

a = sys.argv
if len(a) != 3: usageAndExit()

#load left and right directory names
ld = a[1]
rd = a[2]

ll = collectFileSet(ld)
rl = collectFileSet(rd)

print("left only, need copy to right")
printSet(ll - rl)
print("right only, need copy to left")
printSet(rl - ll)
print("files on both sides")
printSet(rl.intersection(ll))

print("\ncompare mtime on common files")
copyInstructions = compareMtime(rl.intersection(ll), ld, rd)

print("\ncopy instructions:")
for inst in copyInstructions:
     print(inst)
















